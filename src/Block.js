import uniqid from 'uniqid';
import lorem from 'lorem-ipsum';

class Block {
  constructor() {
    this.id = uniqid();
    this.type = 'simple';
    this.text = lorem({ sentenceUpperBound: 8 });
  }
}

class ExtendedBlock extends Block {
  constructor() {
    super();
    this.type = 'extended';
    this.color = false; // false for red, true for green
  }

  toggleColor = () => {
    this.color = !this.color;
  }
}

export { Block, ExtendedBlock };
