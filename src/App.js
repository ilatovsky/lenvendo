import _ from 'lodash';
import { Block, ExtendedBlock } from './Block';
import renderBlock from './BlockTemplate.pug';
import blocksStatus from './BlocksStatus.pug';

let instance = null;
const blocks = [];


let clickCount = 0;
let singleClickTimer = null;

const findBlockInEvent = event => event.path[_.findIndex(event.path,
  (item) => {
    if (item.classList) return item.classList.contains('Block');
    return false;
  })];

const updateStatus = () => {
  const blocksContainer = document.querySelector('.Status');
  const total = blocks.length;
  const selected = document.querySelectorAll('.Block--selected').length;
  const selectedGreen = document.querySelectorAll('.Block--selected.Block--alternativeColor').length;
  const selectedRed = document.querySelectorAll('.Block--selected.Block--extended').length - selectedGreen;
  blocksContainer.innerHTML = blocksStatus({
    total, selected, selectedRed, selectedGreen,
  });
};

const removeBlock = (element) => {
  const { id } = element.parentNode.dataset;
  const index = _.findIndex(blocks, block => block.id === id);
  if (blocks[index].type === 'simple') {
    element.parentNode.remove();
    blocks.splice(index, 1);
  } else if (confirm('Удалить сложный блок?')) {
    element.parentNode.remove();
    blocks.splice(index, 1);
  }
  updateStatus();
};

const selectBlock = (event) => {
  const block = findBlockInEvent(event);
  if (block) block.classList.toggle('Block--selected');
  updateStatus();
};

const toggleBlockColor = (event) => {
  const blockElement = findBlockInEvent(event);
  blockElement.classList.toggle('Block--alternativeColor');
  const block = blocks[_.findIndex(blocks, item => item.id === blockElement.dataset.id)];
  block.toggleColor();
  updateStatus();
};

class App {
  constructor() {
    this.ui = false;
    if (!instance) {
      instance = this;
    }
    return instance;
  }

  initUI = ({
    addBlockButtonClass: addBlockButtonClass = 'AddBlockButton',
    blocksContainerClass: blocksContainerClass = 'BlocksContainer',
  }) => {
    const addBlockButtons = document.querySelectorAll(`.${addBlockButtonClass}`);
    const blocksContainer = document.querySelector(`.${blocksContainerClass}`);
    if (addBlockButtons && blocksContainer) {
      this.ui = { addBlockButtons, blocksContainer };
      addBlockButtons.forEach(button => button.addEventListener('click',
        e => this.addBlock(e.target.dataset.extended)));
      blocksContainer.addEventListener('click',
        e => this.handleBlocksContainerClick(e));
      updateStatus();
    } else console.log('No reqired UI elements in DOM');
  }

  handleBlocksContainerClick = (event) => {
    if (event.path[0].className === 'Block__Close') {
      removeBlock(event.path[0]);
    } else {
      clickCount += 1;
      if (clickCount === 1) {
        singleClickTimer = setTimeout(() => {
          clickCount = 0;
          this.BlockContainerClick(event);
        }, 250);
      } else if (clickCount === 2) {
        clearTimeout(singleClickTimer);
        clickCount = 0;
        this.BlockContainerDoubleClick(event);
      }
    }
  }

  BlockContainerClick = (event) => {
    selectBlock(event);
  }

  BlockContainerDoubleClick = (event) => {
    toggleBlockColor(event);
  }

  addBlock = (extended) => {
    const block = (extended) ? new ExtendedBlock() : new Block();
    blocks.push(block);
    this.ui.blocksContainer.insertAdjacentHTML('beforeend', renderBlock({ block }));
    updateStatus();
  };

  getBlocks = () => blocks;
}

export default new App();
